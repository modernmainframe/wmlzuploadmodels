#!/bin/env python
import json
import requests
import sys

def scoring(inp):
    with open('../token.json') as f:
        data = json.load(f)

    token = data['token']

    with open('../scoringURL.json') as f:
        data = json.load(f)

    SCORING_URL = data["scoring_url"]

    print(SCORING_URL)
    header = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
            'ML-Instance-Id' : 'ibmuser'
    }

    filename = '../testDatas/' + inp
    with open(filename) as f:
        data = json.load(f)

    payload = data['payload']
    response = requests.post(SCORING_URL, json=payload, headers=header,verify=False)

    return(json.loads(response.text)[0]['prediction'])

if __name__ == '__main__':
    for i in range(1,len(sys.argv)):
        print(scoring(sys.argv[i]))
