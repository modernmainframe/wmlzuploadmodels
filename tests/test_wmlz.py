import pytest
from wmlz_test import scoring

def test_wmlz1():
    assert scoring('testData1.json') ==  0.0 
    
def test_wmlz2():
    assert scoring('testData2.json') ==  1.0 

def test_wmlz3():
    assert scoring('testData3.json') == 1.0
    
def test_wmlz4():
    assert scoring('testData4.json') == 0.0

def test_wmlz5():
    assert scoring('testData5.json') == 0.0
