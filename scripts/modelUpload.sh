#!/bin/env sh

NOTEBOOK_NAME=$1
IML_HOME=$2

$IML_HOME/imlpython/env/mlzenv/bin/jupyter nbconvert --execute ${WORKSPACE}/notebooks/$NOTEBOOK_NAME --ExecutePreprocessor.timeout=6000 --inplace

cat ${WORKSPACE}/stdout.txt

echo "Model uploaded to WMLz successfully"

