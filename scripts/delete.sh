#!/bin/env sh

TOKEN=$(cat token.json | sed -n 's/.*"'token'": *"\([^"]*\)".*/\1/p' "token.json" || true )

/u/wml/jenkinsPipeline/extract.sh

python /u/wml/jenkinsPipeline/deploy.py $ARTIFACT_VERSION $MODEL_URL

